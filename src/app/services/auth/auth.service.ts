import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable, of } from 'rxjs';
import { TokenService } from '../token/token.service';
import { User } from 'src/app/models/model';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient, private token: TokenService) { }


  login(user: User): Observable<any> {
    const endPoint: string = environment.endPoint + 'account/login';
    return this.http.post(endPoint, user);
  }

  public logout(): Observable<boolean> {
    if (this.token.UserToken) {
      this.token.UserToken = null;
    }
    return of(true);
  }
}
