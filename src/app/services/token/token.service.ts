import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TokenService {

  private keyUserToken = '__USERTOKEN__';
  constructor() { }

  public get UserToken(): string {
    let value: any;

    if (localStorage.getItem(this.keyUserToken)) {
      value = localStorage.getItem(this.keyUserToken);
    }

    return value;
  }

  public set UserToken(value: string) {
    if (value === null) {
      localStorage.removeItem(this.keyUserToken);
    } else {
      localStorage.setItem(this.keyUserToken, value);
    }
  }
}
