import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { TokenService } from '../token/token.service';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  constructor(private http: HttpClient, private token: TokenService) { }

  getChartData(): Observable<any> {
    const endPoint: string = environment.endPoint + 'dashboard';

    const header = new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + this.token.UserToken
    });

    const options = { headers: header };

    return this.http.get(endPoint, options);
  }
}
