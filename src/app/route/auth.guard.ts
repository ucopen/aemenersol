import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';
import { TokenService } from '../services/token/token.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private token: TokenService, private router: Router) {

  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean {

    const helper = new JwtHelperService();

    const decodedToken = helper.decodeToken(this.token.UserToken);
    const isExpired = helper.isTokenExpired(this.token.UserToken);

    // console.log(decodedToken.sub, isExpired);

    if (decodedToken && !isExpired) {
      return true;
    } else {
      this.router.navigate(['/login'], { replaceUrl: true });
    }
    return false;
  }
}
