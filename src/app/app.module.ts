import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './route/app-routing.module';
import { AppComponent } from './app.component';
import { TokenService } from './services/token/token.service';
import { AuthService } from './services/auth/auth.service';
import { DashboardService } from './services/dashboard/dashboard.service';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [TokenService, AuthService, DashboardService],
  bootstrap: [AppComponent]
})
export class AppModule { }
