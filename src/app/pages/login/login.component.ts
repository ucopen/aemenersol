import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth/auth.service';
import { TokenService } from 'src/app/services/token/token.service';
import { Router } from '@angular/router';
import { User } from 'src/app/models/model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})
export class LoginComponent implements OnInit {
  user: User = { username: '', password: '' };

  constructor(private auth: AuthService, private token: TokenService, private router: Router) { }

  ngOnInit() {
  }

  login() {
    this.auth.login(this.user).subscribe(data => {
      if (data) {
        this.token.UserToken = data;
        this.router.navigate(['/dashboard'], { replaceUrl: true });
      }
    }, err => {
      alert('Not Authorized');
      console.log(err.error);
    });
  }

}
