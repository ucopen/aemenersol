import { Component, OnInit, NgZone, OnDestroy, AfterViewInit } from '@angular/core';
import { DashboardService } from 'src/app/services/dashboard/dashboard.service';

import * as am4core from '@amcharts/amcharts4/core';
import * as am4charts from '@amcharts/amcharts4/charts';
import { AuthService } from 'src/app/services/auth/auth.service';
import { Router } from '@angular/router';
// import am4themes_animated from '@amcharts/amcharts4/themes/animated';

function am4themes_myTheme(target) {
  if (target instanceof am4core.ColorSet) {
    target.list = [
      am4core.color('#9e9e9e'),
      am4core.color('#aeaeae'),
      am4core.color('#bebebe'),
      am4core.color('#cdcdcd'),
      am4core.color('#383838'),
      am4core.color('#191818')
    ];
  }
}

am4core.useTheme(am4themes_myTheme);

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.sass']
})
export class DashboardComponent implements OnInit, AfterViewInit, OnDestroy {
  private pieChart: am4charts.PieChart;
  private barChart: am4charts.XYChart;

  tableData = [];
  constructor(private dashboard: DashboardService, private zone: NgZone, private auth: AuthService, private router: Router) { }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.dashboard.getChartData().subscribe(data => {
      this.zone.runOutsideAngular(() => {
        this.generatePieChart(data.chartDonut);
        this.generateBarChart(data.chartBar);
      });
      this.tableData = data.tableUsers;
    });
  }

  ngOnDestroy() {
    this.zone.runOutsideAngular(() => {
      if (this.pieChart) {
        this.pieChart.dispose();
        this.barChart.dispose();
      }
    });
  }

  generatePieChart(data) {

    this.pieChart = am4core.create('piechartdiv', am4charts.PieChart);
    this.pieChart.data = data;
    this.pieChart.innerRadius = am4core.percent(40);

    const pieSeries = this.pieChart.series.push(new am4charts.PieSeries());
    pieSeries.dataFields.value = 'name';
    pieSeries.dataFields.category = 'value';
  }

  generateBarChart(data) {
    this.barChart = am4core.create('barchartdiv', am4charts.XYChart);
    this.barChart.data = data;

    const categoryAxis = this.barChart.xAxes.push(new am4charts.CategoryAxis());
    categoryAxis.dataFields.category = 'name';
    const valueAxis = this.barChart.yAxes.push(new am4charts.ValueAxis());

    const series = this.barChart.series.push(new am4charts.ColumnSeries());
    series.dataFields.valueY = 'value';
    series.dataFields.categoryX = 'name';
  }


  logout() {
    this.auth.logout().subscribe(() => {
      this.router.navigate(['/login'], { replaceUrl: true });
    });
  }

}
